<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class ShorttLink_Plugin
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Plugin_Name_Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $plugin_name The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $version The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        if (defined('PLUGIN_NAME_VERSION')) {
            $this->version = PLUGIN_NAME_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->plugin_name = 'plugin-name';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();


    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Plugin_Name_Loader. Orchestrates the hooks of the plugin.
     * - Plugin_Name_i18n. Defines internationalization functionality.
     * - Plugin_Name_Admin. Defines all hooks for the admin area.
     * - Plugin_Name_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-plugin-name-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-plugin-name-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-plugin-name-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-shorttlink-plugin-public.php';

        $this->loader = new Plugin_Name_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Plugin_Name_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale()
    {

        $plugin_i18n = new Plugin_Name_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks()
    {

        $plugin_admin = new Plugin_Name_Admin($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
        $this->loader->add_action('admin_menu', $plugin_admin, 'addPluginTabToAdminMenu');


    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks()
    {

        $plugin_public = new ShorttLinkPlugin_Public($this->get_plugin_name(), $this->get_version());


        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
        $this->loader->add_action('template_redirect', $plugin_public, 'redirectToLink');
    }


    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {

//        $this->loader->add_action('init', '');
        $this->loader->run();

        if (!is_admin() && $GLOBALS['pagenow'] !== 'wp-login.php') {

            $this->getForm();
            $this->getShortLink();
            if (!session_id()) {
                session_start();
            }
            if (!isset($_SESSION['short_links'])) {
                $_SESSION['short_links'] = [];
            } else $this->getSavedLinks();

        }


    }


    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @return    string    The name of the plugin.
     * @since     1.0.0
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    public function getForm()
    {

        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/form.php';

    }

    public function getSavedLinks()
    {
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/your_links.php';
    }

    public function getRedirectedLink($link)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'short_links';
        $sql = "SELECT * FROM $table_name";
        $data = $wpdb->get_results($sql);

        $fl = false;
        $search = '';
        foreach ($data as $record) {
            if ($link == $record->short_link) {
                $search = $record;
                break;
            }
        }
        if ($search) {
            return $this->refactorURL($search->link);
        }

    }

    public function getAllLinks()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'short_links';
        $sql = "SELECT * FROM $table_name";
        $data = $wpdb->get_results($sql);
        return $data;
    }

    private function refactorURL($link)
    {
        if (!stristr($link, 'https://')) {
            if (!stristr($link, 'http://')) {
                $link = 'http://' . $link;
            }
        }
        return $link;
    }

    public function getShortLink()
    {
        if (isset($_GET['link'])) {
            $link = $_GET['link'];
            if ($link != '') {
                if ($old_link = $this->checkGenerated($link)) {
                    $this->showLink($link, $old_link['short_link']);
                } else {
                    $short_link = $this->getNewLink($link);
                    if ($short_link != null) {
                        $short_link = home_url() . $short_link;
                        array_push($_SESSION['short_links'], ['short_link' => $short_link, 'link' => $link]);
                        $this->showLink($link, $short_link);
                    }
                    else echo "NOT VALID URL";
                }
            }
        }
    }

    private function checkGenerated($link)
    {
        if (!session_id()) {
            session_start();

        }
        if (!isset($_SESSION['short_links'])) {
            $_SESSION['short_links'] = [];
        }

        $old_link = '';
        foreach ($_SESSION['short_links'] as $record) {
            if ($record['link'] == $link) {
                $old_link = $record;
            }
        }
        return $old_link;
    }

    private function showLink($link, $short_link)
    {
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/result.php';
    }

    private function getNewLink($link)
    {
        if (!preg_match('/^.*\.{1}.{1,}$/', $link)) return null;
        global $wpdb;
        $table_name = $wpdb->prefix . 'short_links';
        $sql = "SELECT * FROM $table_name";
        $data = $wpdb->get_results($sql);

        $new_link = "/" . $this->generateRandomString(6);

        $fl = true;

        foreach ($data as $record) {
            if ($new_link == $record->link) {
                $fl = false;
            }
            if (!$fl) $new_link = "/" . $this->generateRandomString(6);
        }


        if ($fl) {
            $sql = "INSERT INTO $table_name (`short_link`, `link`) VALUES ('$new_link','$link')";
            $wpdb->get_results($sql);
            return $new_link;
        }
    }

    private function generateRandomString($length = 6)
    {


        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @return    Plugin_Name_Loader    Orchestrates the hooks of the plugin.
     * @since     1.0.0
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @return    string    The version number of the plugin.
     * @since     1.0.0
     */
    public function get_version()
    {
        return $this->version;
    }

}
