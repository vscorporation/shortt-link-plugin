<div class="wrap" style="padding: 50px" >
    <div>
        Enter URL:
    </div>
    <form  action="<?= esc_url($_SERVER['REQUEST_URI']) ?>">
        <input type="text" name="link" required>
        <button type="submit">Submit</button>
    </form>
</div>
<style>
    div#page {
        display: none;
    }
    #shortlink-container {
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
    }
</style>