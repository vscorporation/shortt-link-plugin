<div class="wrap" style="padding: 20px">
    YOUR SHORT LINK TO <b><?= $link ?></b>:<br>
    <a target="_blank" id='text_link' href="<?= $short_link ?>"><?= $short_link ?></a>
    <input type="text" value="<?= $short_link ?>" id="myInput"  >

    <!-- The button used to copy the text -->
    <button id="copy_btn" onclick="myFunction()">Copy</button>
</div>
<script>
    function myFunction() {
        var copyText = document.getElementById("myInput");
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        document.getElementById('copy_btn').classList.add("copied");
        document.getElementById('copy_btn').innerHTML = 'Copied';
    }

</script>
<style>
    #copy_btn {
        width: 150px ;
        padding: 5px;
    }
    .copied {
        background: green !important;
    }
    #myInput {
        opacity: 1;
        position: absolute;
        top: -10000000000000px;
    }
</style>