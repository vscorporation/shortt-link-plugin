<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class ShorttLinkPlugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
        global $wpdb;

        $table_name = $wpdb->prefix . 'short_links';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "DROP TABLE IF EXISTS $table_name";
        $wpdb->get_results($sql);
        if( !session_id() )
        {
            session_start();
            $_SESSION['short_links'] = [];
         }
        $_SESSION['short_links'] = [];
	}

}
