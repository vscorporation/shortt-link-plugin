<?php

if (!session_id()) {
    session_start();
}
$short_links = [];
if (isset($_SESSION['short_links']) && count($_SESSION['short_links']) != 0) {
    $short_links = array_reverse( $_SESSION['short_links']);

    echo "<div>Your links:</div>";

}
?>
<?php if(isset($_SESSION['short_links'])) : ?>

<div class="wrap" style="padding: 20px">
<?php foreach($short_links as $link): ?>

<div class="link-block"><div ><?= $link['link'] . ": "?></div>
    <a href="<?= $link['short_link'] ?>"><?= $link['short_link']?></a>
</div>

<?php endforeach;?>
</div>
<style>
    .link-block >div {
        font-weight: bold;
    }
    .link-block a  {
        padding-left: 20px;

    }
</style>
<?php endif;?>