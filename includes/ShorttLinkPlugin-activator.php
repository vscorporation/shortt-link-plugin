<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class ShorttLink_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
        // create the custom table
        global $wpdb;

        $table_name = $wpdb->prefix . 'short_links';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
        id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
        short_link varchar(50) default '',
        link varchar(500) NOT NULL default '',
        date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ) $charset_collate;";
        $wpdb->get_results($sql);
//    dbDelta( $sql );
        if( !session_id() )
        {
            session_start();
            $_SESSION['short_links'] = [];
        }
        $_SESSION['short_links'] = [];
	}

}
